package course.example.actionbar;

import android.content.Intent;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.Menu;
import android.view.MenuInflater;
import android.view.MenuItem;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.TabHost;

public class MainActivity extends AppCompatActivity {
    public final static String EXTRA_MESSAGE="course.example.actionbar.MESSAGE";
    TabHost tabHost;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        setContentView(R.layout.activity_main);
        tabHost= (TabHost)findViewById(R.id.tabHost);
        tabHost.setup();

        TabHost.TabSpec tab1 = tabHost.newTabSpec("tab1");
        TabHost.TabSpec tab2 = tabHost.newTabSpec("tab2");
        TabHost.TabSpec tab3 = tabHost.newTabSpec("tab3");

        tab1.setIndicator("°F - °C");
        tab1.setContent(R.id.tab_1);

        tab2.setIndicator("AREA");
        tab2.setContent(R.id.tab_2);

        tab3.setIndicator("MILLAS - KM");
        tab3.setContent(R.id.tab_3);

        tabHost.addTab(tab1);
        tabHost.addTab(tab2);
        tabHost.addTab(tab3);
    }

    public void calcularGrados(View view){
        EditText edittext = (EditText)findViewById(R.id.cantUsuario1);
        Double resultado = ((Double.valueOf(edittext.getText().toString())-32)/1.8);
        Intent intent = new Intent(this,DisplayMessageActivity.class);
        intent.putExtra(EXTRA_MESSAGE,resultado.toString());
        startActivity(intent);
    }

    public void calcularArea(View view){
        EditText edittext = (EditText)findViewById(R.id.cantUsuario2);
        Double resultado = (Double.valueOf(edittext.getText().toString()))*(Double.valueOf(edittext.getText().toString()));
        Intent intent = new Intent(this,DisplayMessageActivity.class);
        intent.putExtra(EXTRA_MESSAGE,resultado.toString());
        startActivity(intent);
    }

    public void calcularMillas(View view){
        EditText edittext = (EditText)findViewById(R.id.cantUsuario3);
        Double resultado = (Double.valueOf(edittext.getText().toString()))*1.62;
        Intent intent = new Intent(this,DisplayMessageActivity.class);
        intent.putExtra(EXTRA_MESSAGE,resultado.toString());
        startActivity(intent);
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        MenuInflater inflater = getMenuInflater();
        inflater.inflate(R.menu.main_activity_actions, menu);
        return super.onCreateOptionsMenu(menu);
    }
    public boolean onOptionsItemSelected(MenuItem item){
        switch(item.getItemId()){
            case R.id.action_search:
                //openSearch();
                return true;
            case R.id.action_settings:
                //openSettings();
                return true;
            default:
                return super.onOptionsItemSelected(item);
        }
    }

}
